# My Old Projects Hosted on SourceForge


_The source codes are hosted on CVS repositories. (Note: I just realized that some projects were empty and the source code had not been uploaded.)_


* [ActiveX Go Board Control](https://sourceforge.net/projects/goboard/)
* [Amylase: Structured File HTML/XMLizer](https://sourceforge.net/projects/amylase/)
* [Blackjack Class Library in .Net](https://sourceforge.net/projects/bjlib/)
* [CVS Client](https://sourceforge.net/projects/cvsclient/)
* [DBPad: Universal DB Client](https://sourceforge.net/projects/dbpad/)
* [DVD Web Service](https://sourceforge.net/projects/dvdweb/)
* [Digital Image Processing Library in C++](https://sourceforge.net/projects/dimple/)
* [Hidden Markov Model Development Kit](https://sourceforge.net/projects/hmmsdk/)
* [Pattern Analysis Library in C++](https://sourceforge.net/projects/palib/)
* [PocketPC Block Game](https://sourceforge.net/projects/pocketblock/)
* [Random Number Service](https://sourceforge.net/projects/randservice/)
* [Servlet Module Development Studio](https://sourceforge.net/projects/servletstudio/)
* [Space Invader Cloning Project](https://sourceforge.net/projects/invader/)
* [Standard Numerical Library in C++](https://sourceforge.net/projects/snl/)
* [Visual Object Simulator](https://sourceforge.net/projects/vissim/)
* [Web Blogging Client](https://sourceforge.net/projects/webblog/)
* [Web Counter Service](https://sourceforge.net/projects/webcounters/)
* [WebStone Web Application Framework](https://sourceforge.net/projects/webstone/)

